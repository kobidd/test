<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Task;
use App\User;
use Illuminate\Support\Facades\Gate;
use Illuminate\Support\Facades\Response;
class TaskController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        
        $tasks=Task::all();
        $id=Auth::id();
        return view('tasks.index',['tasks'=>$tasks],['id'=>$id]);//showing all the speciped user tasks
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('tasks.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $id=Auth::id();
        $tasks = new Task();
        $tasks->title = $request->title;
        $tasks->user_id = $id;
        $tasks->status =0;
        $tasks->save();
        return redirect('tasks');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $tasks = Task::find($id);
        return view('tasks.edit',['tasks'=>$tasks]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $tasks = Task::findOrFail($id);
        if (Gate::denies('admin')) {
            if ($request->has('title'))
                   abort(403,"You are not allowed to edit tasks..");
        }    
        $tasks -> update($request->all());
        return redirect('tasks');

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        if (Gate::denies('admin')) {
            abort(403,"Sorry you are not allowed to delete tasks..");
        } 
        $tasks = Task::find($id);
        $tasks -> delete();
        return redirect('tasks');
    }
    public function upd($id){
        $tasks = Task::find($id);
        if($tasks->status==0)
            $tasks->status =1;
        $tasks->update();   
        return redirect('tasks');
    }
    public function mytask(){
        $id = Auth::id();
        $tasks = User::find($id)->tasks;
        return view('tasks.mytask', ['tasks'=>$tasks]);
      
    }
}

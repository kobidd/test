@extends('layouts.app')
@section('content')
<a href="{{route('index')}}">show all tasks<a>
<h1>This is the Task list</h1>
<ul>
    @foreach($tasks as $task)
    <li>
        id: {{$task->id}} title:    {{$task->title}} <a href= "{{route('tasks.edit', $task->id )}}">Edit Task</a>
        @cannot('user')<a href="{{route('delete',$task->id)}}">Delete</a>@endcannot
        @cannot('user') @if($task->status)
        <a>Mark as done</a>
        @else
        <a href="{{route('update',$task->id)}}">Mark as done</a>
        @endif
        @endcannot
    @endforeach
</ul>
@endsection
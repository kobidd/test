<h1>Edit Task</h1>
<form method = 'post' action="{{action('TaskController@update', $tasks->id)}}">
@csrf
@method('PATCH')
<div class = "form-group">
    <label for = "title">Task to Update:</label>
    <input type= "text" class = "form-control" name= "title" value = "{{$tasks->title}}">
</div>

<div class = "form-group">
    <input type ="submit" class = "form-control" name="submit" value ="Just Do It">
</div>
</form>
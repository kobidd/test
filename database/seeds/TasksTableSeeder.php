<?php

use Illuminate\Database\Seeder;

class TasksTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('tasks')->insert(
            [
                [
                        'title' => 'task1',
                        'created_at' => date('Y-m-d G:i:s'),
                         'user_id'=>'1',
                ],
                [
                    'title' => 'task2',
                    'created_at' => date('Y-m-d G:i:s'),
                    'user_id'=>'1',
            ],
            [
                'title' => 'task3',
                'created_at' => date('Y-m-d G:i:s'),
                'user_id'=>'1',
        ],
            ]); 
    }
}
